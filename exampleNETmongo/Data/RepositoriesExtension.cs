﻿using exampleNETmongo.Data.Repository.Abstract;
using exampleNETmongo.Data.Repository.Implements;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace exampleNETmongo.Data
{
    public static class RepositoriesExtension
    {
        public static void RegisterRepos(this IServiceCollection services)
        {
            services.AddTransient<IPersonRepository, PersonRepository>();
        }
    }
}
