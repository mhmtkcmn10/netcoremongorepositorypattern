﻿using Entities.BusinessEntities.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace exampleNETmongo.Data.Repository.Abstract
{
    public interface IBaseRepository<T> where T : BaseEntity
    {
        List<T> findAll();
        T find(string id);

        void create(T entity);

        void update(string id, T entity);

        void delete(string id);




    }
}
