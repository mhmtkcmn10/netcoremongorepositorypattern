﻿using exampleNETmongo.Entities.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace exampleNETmongo.Data.Repository.Abstract
{
    public interface IPersonRepository:IBaseRepository<Person>
    {
    }
}
