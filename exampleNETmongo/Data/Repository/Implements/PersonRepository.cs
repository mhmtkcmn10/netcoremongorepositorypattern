﻿using exampleNETmongo.Data.Repository.Abstract;
using exampleNETmongo.Entities.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace exampleNETmongo.Data.Repository.Implements
{
    public class PersonRepository : BaseRepository<Person>, IPersonRepository
    {
        public PersonRepository(IMongoDBContext context) : base(context)
        {

        }
    }
}
