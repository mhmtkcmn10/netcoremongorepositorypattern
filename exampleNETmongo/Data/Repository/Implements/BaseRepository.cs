﻿using Entities.BusinessEntities.Base;
using exampleNETmongo.Data.Repository.Abstract;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace exampleNETmongo.Data.Repository.Implements
{
    public abstract class BaseRepository<T> : IBaseRepository<T> where T : BaseEntity
    {
        protected readonly IMongoDBContext _mongoContext;
        protected IMongoCollection<T> _dbCollection;

        protected BaseRepository(IMongoDBContext context)
        {
            _mongoContext = context;
            _dbCollection = _mongoContext.GetCollection<T>(typeof(T).Name);
        }
        public void create(T entity)
        {
            _dbCollection.InsertOne(entity);
        }

        public void delete(string id)
        {
            var docId = new ObjectId(id);
            _dbCollection.DeleteOne(x=>x.Id==docId);
        }

        public T find(string id)
        {
            var docId = new ObjectId(id);
            return _dbCollection.Find<T>(m => m.Id == docId).FirstOrDefault();
        }

        public List<T> findAll()
        {
            return _dbCollection.AsQueryable<T>().ToList();
        }

        public void update(string id, T entity)
        {
            var docId = new ObjectId(id);
            _dbCollection.ReplaceOne(m => m.Id == docId, entity);
        }
    }
}
