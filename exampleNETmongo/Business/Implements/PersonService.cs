﻿using exampleNETmongo.Business.Abstract;
using exampleNETmongo.Data.Repository.Abstract;
using exampleNETmongo.Entities.BusinessEntities;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace exampleNETmongo.Business.Implements
{
    public class PersonService : IPersonService
    {
        IPersonRepository _personRepository;
        public PersonService(IPersonRepository personRepository)
        {
            _personRepository = personRepository;
        }

        public void create(Person person)
        {
            _personRepository.create(person);
        }

        public void delete(string id)
        {
            _personRepository.delete(id);
        }

        public Person find(string id)
        {
            return _personRepository.find(id);
        }

        public List<Person> findAll()
        {
            return _personRepository.findAll();

        }

        public void update(string id, Person person)
        {
            _personRepository.update(id,person);
        }
    }
}
