﻿using exampleNETmongo.Entities.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace exampleNETmongo.Business.Abstract
{
    public interface IPersonService
    {
        List<Person> findAll();

        Person find(string id);

        void create(Person person);

        void delete(string id);

        void update(string id, Person person);
    }
}
