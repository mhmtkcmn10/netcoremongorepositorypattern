﻿using exampleNETmongo.Business.Abstract;
using exampleNETmongo.Business.Implements;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace exampleNETmongo.Business
{
    public static class ServicesExtension
    {
        public static void RegisterServices(this IServiceCollection services)
        {
            services.AddTransient<IPersonService, PersonService>();
        }


    }
}
