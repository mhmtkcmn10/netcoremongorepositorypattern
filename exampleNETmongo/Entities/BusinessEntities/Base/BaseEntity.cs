﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.BusinessEntities.Base
{
    public interface BaseEntity
    {
        [BsonId]
        public ObjectId Id { get; set; }

    }
}
