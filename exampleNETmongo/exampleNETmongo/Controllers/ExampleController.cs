﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using exampleNETmongo.Business.Abstract;
using exampleNETmongo.Business.Implements;
using exampleNETmongo.Entities.BusinessEntities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;

namespace exampleNETmongo.Controllers
{
    public class ExampleController : Controller
    {
        private IPersonService _personService;

        public ExampleController(IPersonService personService)
        {
            _personService = personService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(Person person)
        {
            _personService.create(person);
            return View();

        }

        public IActionResult Listele()
        {
            var list = _personService.findAll().ToList();

            return View(list);
        }

        public IActionResult Edit(string id)
        {
            Person person = _personService.find(id);

            return View(person);
        }

        [HttpPost]
        public IActionResult Edit(string id,Person person)
        {
            person.Id = new ObjectId(id);
            _personService.update(id,person);

            return RedirectToAction("Listele");
        }

        public IActionResult Delete(string id)
        {
            Person person = _personService.find(id);

            return View(person);
        }

        [HttpPost]
        public IActionResult Delete(string id, Person person)
        {
            _personService.delete(id);

            return RedirectToAction("Listele");
        }


        public IActionResult Details(string id)
        {
            Person person = _personService.find(id);

            return View(person);
        }



    }
}
